
--PUNTO 5 -> BAJAS DE RENTAS
--DROP FUNCTION baja_renta
--no nos salio la declaracion de variables, nunca hemos usado postgres
CREATE OR REPLACE FUNCTION baja_renta(id_inventario INT, id_cliente INT, id_trabajador INT)
RETURNS void AS $$
BEGIN
  IF EXISTS (SELECT 1 FROM customer WHERE customer_id = id_cliente AND active = 1) THEN
    IF EXISTS (SELECT 1 FROM staff WHERE staff_id = id_trabajador AND active = true) THEN
    
      IF EXISTS (SELECT 1 FROM inventory WHERE inventory_id IN (select get_rentados()) AND inventory_id = id_inventario) THEN
	    IF EXISTS (SELECT 1 FROM rental WHERE inventory_id = id_inventario AND customer_id = id_cliente AND return_date is null) THEN
	
          INSERT INTO payment (customer_id, staff_id, rental_id, amount, payment_date) 
          VALUES (id_cliente, 
           id_trabajador, 
           (SELECT rental_id FROM rental WHERE inventory_id = id_inventario AND customer_id = id_cliente AND return_date is null), 
           0.99+(SELECT EXTRACT(DAY FROM NOW() - (SELECT rental_date FROM rental 
              WHERE inventory_id = id_inventario 
              AND customer_id = id_cliente 
              AND return_date is null))::int
           ),
           now());

          UPDATE rental 
          SET return_date = now()
          WHERE rental_id = (SELECT rental_id FROM rental WHERE inventory_id = id_inventario AND customer_id = id_cliente AND return_date is null);
        ELSE
          RAISE EXCEPTION 'No existen registros de esa renta.';
        END IF;
      ELSE
        RAISE EXCEPTION 'El film no esta rentado o no existe.';
      END IF;
    ELSE
      RAISE EXCEPTION 'El id del trabajador no existe o esta dado de baja.';
    END IF;
  ELSE
    RAISE EXCEPTION 'El id de usuario no existe o esta dado de baja.';
  END IF;
END; $$
LANGUAGE 'plpgsql';
--SELECT baja_renta(4580, 200, 2)