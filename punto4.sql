--PUNTO 4 -> ALTAS DE RENTAS
--DROP FUNCTION alta_renta
CREATE OR REPLACE FUNCTION alta_renta(id_inventario INT, id_cliente INT, id_trabajador INT)
RETURNS void AS $$
BEGIN
  IF EXISTS (SELECT 1 FROM customer WHERE customer_id = id_cliente AND active = 1) THEN
    IF EXISTS (SELECT 1 FROM staff WHERE staff_id = id_trabajador AND active = true) THEN
      IF EXISTS (SELECT 1 FROM inventory WHERE inventory_id NOT IN (select get_rentados()) AND inventory_id = id_inventario) THEN
        INSERT INTO rental (rental_date, inventory_id, customer_id, staff_id, last_update) 
        VALUES (now(), id_inventario, id_cliente, id_trabajador, now());
      ELSE
        RAISE EXCEPTION 'El film ya esta rentado o no existe.';
      END IF;
    ELSE
      RAISE EXCEPTION 'El id del trabajador no existe o esta dado de baja.';
    END IF;
  ELSE
    RAISE EXCEPTION 'El id de usuario no existe o esta dado de baja.';
  END IF;
END; $$
LANGUAGE 'plpgsql';
--SELECT alta_renta(4580, 200, 2)