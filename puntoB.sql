--PUNTO 1 -> b -> ALTAS ACTOR
--DROP FUNCTION agrega_actor
--los actores pueden tener nombre y apellidos iguales (es posible)
CREATE OR REPLACE FUNCTION agrega_actor(nombre VARCHAR(45), apellido VARCHAR(45)) 
RETURNS void AS $$
BEGIN
  INSERT INTO actor (first_name, last_name, last_update) VALUES (nombre, apellido, now());
END; $$ 
LANGUAGE 'plpgsql';
--SELECT agrega_actor('HECTOR', 'CAZAREZ') 



--PUNTO 1 -> b -> CAMBIOS ACTOR
--DROP FUNCTION update_actor
CREATE OR REPLACE FUNCTION update_actor(id INT, nombre VARCHAR(45), apellido VARCHAR(45)) 
RETURNS void AS $$
BEGIN
  IF EXISTS (SELECT 1 FROM actor WHERE actor_id = id) THEN
    UPDATE actor 
    SET first_name = nombre, last_name = apellido, last_update = now()
    WHERE actor_id = id;
  ELSE
    RAISE EXCEPTION 'El id del actor no existe.';
  END IF;
END; $$ 
LANGUAGE 'plpgsql';

--SELECT update_actor(205, 'ALEX', 'RODRIGUEZ') 



--PUNTO 1 -> b -> BAJAS ACTOR
--DROP FUNCTION delete_actor
CREATE OR REPLACE FUNCTION delete_actor(id INT) 
RETURNS void AS $$
BEGIN
  IF EXISTS (SELECT 1 FROM film_actor WHERE actor_id = id) THEN
    RAISE EXCEPTION 'El actor esta ligado a alguna pelicula, no se puede eliminar.';
  ELSE
    IF EXISTS (SELECT 1 FROM actor WHERE actor_id = id) THEN
      DELETE FROM actor WHERE actor_id = id;
    ELSE
      RAISE EXCEPTION 'El id del actor no existe.';
    END IF;
  END IF;
END; $$ 
LANGUAGE 'plpgsql';
--SELECT delete_actor(200)