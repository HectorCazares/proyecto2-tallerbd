--PUNTO 1 -> a -> ALTAS CATEGORIA
--DROP FUNCTION agrega_categoria
CREATE OR REPLACE FUNCTION agrega_categoria(nombre VARCHAR(25)) 
RETURNS void AS $$
BEGIN
  IF EXISTS (SELECT 1 FROM category WHERE name ILIKE nombre) THEN
    RAISE EXCEPTION 'La categoria ya existe.';
  ELSE
    INSERT INTO category (name, last_update) VALUES (nombre, now());
  END IF;
END; $$ 
LANGUAGE 'plpgsql';
--SELECT agrega_categoria('Action')

--PUNTO 1 -> a -> CAMBIOS CATEGORIA
--DROP FUNCTION update_categoria
CREATE OR REPLACE FUNCTION update_categoria(id INT, nombre VARCHAR(25)) 
RETURNS void AS $$
BEGIN
  IF EXISTS (SELECT 1 FROM category WHERE category_id = id) THEN
    UPDATE category 
    SET name = nombre, last_update = now()
    WHERE category_id = id;
  ELSE
    RAISE EXCEPTION 'El id del la categoria no existe.';
  END IF;
END; $$ 
LANGUAGE 'plpgsql';
--SELECT update_categoria(20, 'Accion') 


--PUNTO 1 -> a -> BAJAS CATEGORIA
--DROP FUNCTION delete_categoria
CREATE OR REPLACE FUNCTION delete_categoria(id INT) 
RETURNS void AS $$
BEGIN
  IF EXISTS (SELECT 1 FROM film_category WHERE category_id = id) THEN
    RAISE EXCEPTION 'La categoria esta ligada a alguna pelicula, no se puede eliminar.';
  ELSE
    IF EXISTS (SELECT 1 FROM category WHERE category_id = id) THEN
      DELETE FROM category WHERE category_id = id;
    ELSE
      RAISE EXCEPTION 'El id de la categoria no existe.';
    END IF;
  END IF;
END; $$ 
LANGUAGE 'plpgsql';
--SELECT delete_categoria (200)