--PUNTO 3
--DROP FUNCTION get_existencia
CREATE OR REPLACE FUNCTION get_existencia (titulo1 VARCHAR) 
  RETURNS TABLE (
  id INT,
  titulo VARCHAR,
  total BIGINT
) 
AS $$
BEGIN
  RETURN QUERY SELECT store.store_id, title, count(*)
  FROM inventory, store, film
  WHERE store.store_id = inventory.store_id
  AND film.film_id = inventory.film_id
  AND title ILIKE titulo1
  AND inventory.inventory_id NOT IN (SELECT * FROM get_rentados())
  GROUP BY store.store_id, title;
END; $$ 
LANGUAGE 'plpgsql';
--SELECT * FROM get_existencia ('%CHE%');

--get_existencia UTILIZA get_rentados Y OBTIENE LOS QUE ESTAN RENTADOS PARA NO CONTARLOS COMO DISPONIBLES
CREATE OR REPLACE FUNCTION get_rentados () 
RETURNS TABLE (
  id_inventario INT
) 
AS $$
BEGIN
  RETURN QUERY SELECT inventory_id
  FROM rental
  WHERE rental_id not in (select payment.rental_id from payment);
END; $$ 
LANGUAGE 'plpgsql';
--SELECT * FROM get_rentados()