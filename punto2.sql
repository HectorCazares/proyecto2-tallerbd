--PUNTO 2
--DROP FUNCTION get_actor
CREATE OR REPLACE FUNCTION get_actor (apellido1 VARCHAR) 
  RETURNS TABLE (
  id INT,
  nombre VARCHAR,
  apellido VARCHAR
) 
AS $$
BEGIN
  RETURN QUERY SELECT
  actor_id,
  first_name,
  last_name
  FROM
  actor
  WHERE
  last_name ILIKE apellido1;
END; $$ 
LANGUAGE 'plpgsql';
--SELECT * FROM get_actor ('%ro%');