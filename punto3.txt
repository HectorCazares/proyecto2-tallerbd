--si se deja solo el porcentaje % muestra todas las peliculas, get_existencia('%')

select * from get_existencia('%chil%')

id      titulo                total
1   "Banger Pinocchio"         2
1   "Chicago North"            2
1   "Chicken Hellfighters"     1
1   "Chill Luck"               3
1   "Chitty Lock"              3
1   "Clones Pinocchio"         1
1   "Hawk Chill"               1
1   "Magnificent Chitty"       2
1   "Nightmare Chill"          2
1   "Pinocchio Simon"          2
1   "Trading Pinocchio"        3
2   "Banger Pinocchio"         1
2   "Chicago North"            2
2   "Chicken Hellfighters"     1
2   "Chill Luck"               3
2   "Chisum Behavior"          1
2   "Clones Pinocchio"         2
2   "Nightmare Chill"          1
2   "Pinocchio Simon"          2
2   "Trading Pinocchio"        2

