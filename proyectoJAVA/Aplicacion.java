package proyectoJAVA;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Container;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.UIManager;
import javax.swing.ButtonGroup;
import java.awt.CardLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Aplicacion {

	private JFrame frame;
	private final ButtonGroup menuPrincipal = new ButtonGroup();
	private JTextField txt_altaActor_nombre;
	private JTextField txt_altaActor_apellido;
	private JTextField txt_bajaActor_id;
	private JTextField txt_cambiarActor_id;
	private JTextField txt_cambiarActor_nombre;
	private JTextField txt_cambiarActor_apellido;
	private JTextField txt_altaCategoria_nombre;
	private JTextField txt_bajaCategoria_id;
	private JTextField txt_cambiarCategoria_id;
	private JTextField txt_cambiarCategoria_nombre;
	private JTextField txt_busquedaApellido;
	private JTable table_actores;
	private JTextField txt_busquedaTitulo;
	private JTable table_existencia;
	private JRadioButtonMenuItem menuRentarRentas, menuDevolverRentas, menuActoresBusqueda, menuExistenciaBusqueda, menuAltaActores, menuBajaActores, menuCambiarActores, menuAltaCategoria, menuBajaCategoria, menuCambiarCategoria;
	private JPanel panel;
	private JTextField txt_renta_idInventario;
	private JTextField txt_renta_idCliente;
	private JTextField txt_renta_idTrabajador;
	private JTextField txt_devolucion_idInventario;
	private JTextField txt_devolucion_idCliente;
	private JTextField txt_devolucion_idTrabajador;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					Aplicacion window = new Aplicacion();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Aplicacion() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		
		selMenu opcion = new selMenu();
		
		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new CardLayout(0, 0));
		
		JPanel panel_altaActor = new JPanel();
		panel_altaActor.setLayout(null);
		panel_altaActor.setBackground(Color.WHITE);
		panel.add(panel_altaActor, "panel_alta_Actor");
		
		JLabel lblAltaActor = new JLabel("Alta Actor");
		lblAltaActor.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblAltaActor.setBounds(15, 8, 177, 20);
		panel_altaActor.add(lblAltaActor);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 35, 372, 2);
		panel_altaActor.add(separator);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombre.setBounds(15, 53, 86, 14);
		panel_altaActor.add(lblNombre);
		
		txt_altaActor_nombre = new JTextField();
		txt_altaActor_nombre.setColumns(10);
		txt_altaActor_nombre.setBounds(111, 48, 267, 25);
		panel_altaActor.add(txt_altaActor_nombre);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setHorizontalAlignment(SwingConstants.RIGHT);
		lblApellido.setBounds(15, 89, 86, 14);
		panel_altaActor.add(lblApellido);
		
		txt_altaActor_apellido = new JTextField();
		txt_altaActor_apellido.setColumns(10);
		txt_altaActor_apellido.setBounds(111, 84, 267, 25);
		panel_altaActor.add(txt_altaActor_apellido);
		
		JButton btnDarDeAlta_actor = new JButton("Dar de alta");
		btnDarDeAlta_actor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (verifica(txt_altaActor_nombre.getText()) && verifica(txt_altaActor_apellido.getText())) {
					String data = PostgreSQLJDBC.consulta(4, new String[] {txt_altaActor_nombre.getText(), txt_altaActor_apellido.getText()});
					JOptionPane.showMessageDialog(null, data);
				} else {
					JOptionPane.showMessageDialog(null, "Complete todos los campos.");
				}
			}
		});
		btnDarDeAlta_actor.setBounds(206, 204, 88, 25);
		panel_altaActor.add(btnDarDeAlta_actor);
		
		JButton btnLimpiar_altaActor = new JButton("Limpiar");
		btnLimpiar_altaActor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_altaActor_nombre.setText("");
				txt_altaActor_apellido.setText("");
			}
		});
		btnLimpiar_altaActor.setBounds(304, 204, 69, 25);
		panel_altaActor.add(btnLimpiar_altaActor);
		
		JPanel panel_bajaActor = new JPanel();
		panel_bajaActor.setLayout(null);
		panel_bajaActor.setBackground(Color.WHITE);
		panel.add(panel_bajaActor, "panel_baja_Actor");
		
		JLabel lblBajaActor = new JLabel("Baja Actor");
		lblBajaActor.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblBajaActor.setBounds(15, 8, 177, 20);
		panel_bajaActor.add(lblBajaActor);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 35, 372, 2);
		panel_bajaActor.add(separator_1);
		
		JLabel lblIdActor_1 = new JLabel("ID actor:");
		lblIdActor_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdActor_1.setBounds(10, 50, 86, 14);
		panel_bajaActor.add(lblIdActor_1);
		
		txt_bajaActor_id = new JTextField();
		txt_bajaActor_id.setColumns(10);
		txt_bajaActor_id.setBounds(106, 45, 267, 25);
		panel_bajaActor.add(txt_bajaActor_id);
		
		JButton btnDarDeBaja_actor = new JButton("Dar de baja");
		btnDarDeBaja_actor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (verifica(txt_bajaActor_id.getText())) {
					String data = PostgreSQLJDBC.consulta(6, new String[] {txt_bajaActor_id.getText()});
					JOptionPane.showMessageDialog(null, data);
				} else {
					JOptionPane.showMessageDialog(null, "Complete todos los campos.");
				}
			}
		});
		btnDarDeBaja_actor.setBounds(207, 204, 88, 25);
		panel_bajaActor.add(btnDarDeBaja_actor);
		
		JButton btnLimpiar_bajaActor = new JButton("Limpiar");
		btnLimpiar_bajaActor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_bajaActor_id.setText("");
			}
		});
		btnLimpiar_bajaActor.setBounds(305, 204, 69, 25);
		panel_bajaActor.add(btnLimpiar_bajaActor);
		
		JPanel panel_cambiarActor = new JPanel();
		panel_cambiarActor.setLayout(null);
		panel_cambiarActor.setBackground(Color.WHITE);
		panel.add(panel_cambiarActor, "panel_cambiar_Actor");
		
		JLabel lblCambiarActor = new JLabel("Cambiar Actor");
		lblCambiarActor.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblCambiarActor.setBounds(15, 8, 177, 20);
		panel_cambiarActor.add(lblCambiarActor);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(10, 35, 372, 2);
		panel_cambiarActor.add(separator_2);
		
		JLabel label_1 = new JLabel("ID actor:");
		label_1.setHorizontalAlignment(SwingConstants.RIGHT);
		label_1.setBounds(10, 50, 86, 14);
		panel_cambiarActor.add(label_1);
		
		txt_cambiarActor_id = new JTextField();
		txt_cambiarActor_id.setColumns(10);
		txt_cambiarActor_id.setBounds(106, 45, 267, 25);
		panel_cambiarActor.add(txt_cambiarActor_id);
		
		JLabel label_2 = new JLabel("Nombre:");
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setBounds(10, 86, 86, 14);
		panel_cambiarActor.add(label_2);
		
		txt_cambiarActor_nombre = new JTextField();
		txt_cambiarActor_nombre.setColumns(10);
		txt_cambiarActor_nombre.setBounds(106, 81, 267, 25);
		panel_cambiarActor.add(txt_cambiarActor_nombre);
		
		JLabel label_3 = new JLabel("Apellido:");
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setBounds(10, 122, 86, 14);
		panel_cambiarActor.add(label_3);
		
		txt_cambiarActor_apellido = new JTextField();
		txt_cambiarActor_apellido.setColumns(10);
		txt_cambiarActor_apellido.setBounds(106, 117, 267, 25);
		panel_cambiarActor.add(txt_cambiarActor_apellido);
		
		JButton btnCambiar_actor = new JButton("Aplicar");
		btnCambiar_actor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (verifica(txt_cambiarActor_id.getText()) && verifica(txt_cambiarActor_nombre.getText()) && verifica(txt_cambiarActor_apellido.getText())) {
					String data = PostgreSQLJDBC.consulta(5, new String[] {txt_cambiarActor_id.getText(), txt_cambiarActor_nombre.getText(), txt_cambiarActor_apellido.getText()});
					JOptionPane.showMessageDialog(null, data);
				} else {
					JOptionPane.showMessageDialog(null, "Complete todos los campos.");
				}
			}
		});
		btnCambiar_actor.setBounds(206, 204, 88, 25);
		panel_cambiarActor.add(btnCambiar_actor);
		
		JButton btnLimpiar_cambiarActor = new JButton("Limpiar");
		btnLimpiar_cambiarActor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_cambiarActor_id.setText("");
				txt_cambiarActor_nombre.setText("");
				txt_cambiarActor_apellido.setText("");
			}
		});
		btnLimpiar_cambiarActor.setBounds(304, 204, 69, 25);
		panel_cambiarActor.add(btnLimpiar_cambiarActor);
		
		JPanel panel_altaCategoria = new JPanel();
		panel_altaCategoria.setLayout(null);
		panel_altaCategoria.setBackground(Color.WHITE);
		panel.add(panel_altaCategoria, "panel_alta_Categoria");
		
		JLabel lblAltaCategoria = new JLabel("Alta Categoria");
		lblAltaCategoria.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblAltaCategoria.setBounds(15, 8, 177, 20);
		panel_altaCategoria.add(lblAltaCategoria);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(10, 35, 372, 2);
		panel_altaCategoria.add(separator_3);
		
		JLabel label_5 = new JLabel("Nombre:");
		label_5.setHorizontalAlignment(SwingConstants.RIGHT);
		label_5.setBounds(15, 53, 86, 14);
		panel_altaCategoria.add(label_5);
		
		txt_altaCategoria_nombre = new JTextField();
		txt_altaCategoria_nombre.setColumns(10);
		txt_altaCategoria_nombre.setBounds(111, 48, 267, 25);
		panel_altaCategoria.add(txt_altaCategoria_nombre);
		
		JButton btnDarDeAlta_categoria = new JButton("Dar de alta");
		btnDarDeAlta_categoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (verifica(txt_altaCategoria_nombre.getText())) {
					String data = PostgreSQLJDBC.consulta(1, new String[] {txt_altaCategoria_nombre.getText()});
					JOptionPane.showMessageDialog(null, data);
				} else {
					JOptionPane.showMessageDialog(null, "Complete todos los campos.");
				}
			}
		});
		btnDarDeAlta_categoria.setBounds(206, 204, 88, 25);
		panel_altaCategoria.add(btnDarDeAlta_categoria);
		
		JButton btnLimpiar_altaCategoria = new JButton("Limpiar");
		btnLimpiar_altaCategoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_altaCategoria_nombre.setText("");
			}
		});
		btnLimpiar_altaCategoria.setBounds(304, 204, 69, 25);
		panel_altaCategoria.add(btnLimpiar_altaCategoria);
		
		JPanel panel_bajaCategoria = new JPanel();
		panel_bajaCategoria.setLayout(null);
		panel_bajaCategoria.setBackground(Color.WHITE);
		panel.add(panel_bajaCategoria, "panel_baja_Categoria");
		
		JLabel lblBajaCategoria = new JLabel("Baja Categoria");
		lblBajaCategoria.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblBajaCategoria.setBounds(15, 8, 177, 20);
		panel_bajaCategoria.add(lblBajaCategoria);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBounds(10, 35, 372, 2);
		panel_bajaCategoria.add(separator_4);
		
		JLabel lblIdCategoria_1 = new JLabel("ID categoria:");
		lblIdCategoria_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdCategoria_1.setBounds(10, 50, 86, 14);
		panel_bajaCategoria.add(lblIdCategoria_1);
		
		txt_bajaCategoria_id = new JTextField();
		txt_bajaCategoria_id.setColumns(10);
		txt_bajaCategoria_id.setBounds(106, 45, 267, 25);
		panel_bajaCategoria.add(txt_bajaCategoria_id);
		
		JButton btnDarDeBaja_categoria = new JButton("Dar de baja");
		btnDarDeBaja_categoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (verifica(txt_bajaCategoria_id.getText())) {
					String data = PostgreSQLJDBC.consulta(3, new String[] {txt_bajaCategoria_id.getText()});
					JOptionPane.showMessageDialog(null, data);
				} else {
					JOptionPane.showMessageDialog(null, "Complete todos los campos.");
				}
			}
		});
		btnDarDeBaja_categoria.setBounds(207, 204, 88, 25);
		panel_bajaCategoria.add(btnDarDeBaja_categoria);
		
		JButton btnLimpiar_bajaCategoria = new JButton("Limpiar");
		btnLimpiar_bajaCategoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_bajaCategoria_id.setText("");
			}
		});
		btnLimpiar_bajaCategoria.setBounds(305, 204, 69, 25);
		panel_bajaCategoria.add(btnLimpiar_bajaCategoria);
		
		JPanel panel_cambiarCategoria = new JPanel();
		panel_cambiarCategoria.setLayout(null);
		panel_cambiarCategoria.setBackground(Color.WHITE);
		panel.add(panel_cambiarCategoria, "panel_cambiar_Categoria");
		
		JLabel lblCambiarCategoria = new JLabel("Cambiar Categoria");
		lblCambiarCategoria.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblCambiarCategoria.setBounds(15, 8, 177, 20);
		panel_cambiarCategoria.add(lblCambiarCategoria);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setBounds(10, 35, 372, 2);
		panel_cambiarCategoria.add(separator_5);
		
		JLabel lblIdCategoria_2 = new JLabel("ID categoria:");
		lblIdCategoria_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdCategoria_2.setBounds(10, 50, 86, 14);
		panel_cambiarCategoria.add(lblIdCategoria_2);
		
		txt_cambiarCategoria_id = new JTextField();
		txt_cambiarCategoria_id.setColumns(10);
		txt_cambiarCategoria_id.setBounds(106, 45, 267, 25);
		panel_cambiarCategoria.add(txt_cambiarCategoria_id);
		
		JLabel label_11 = new JLabel("Nombre:");
		label_11.setHorizontalAlignment(SwingConstants.RIGHT);
		label_11.setBounds(10, 86, 86, 14);
		panel_cambiarCategoria.add(label_11);
		
		txt_cambiarCategoria_nombre = new JTextField();
		txt_cambiarCategoria_nombre.setColumns(10);
		txt_cambiarCategoria_nombre.setBounds(106, 81, 267, 25);
		panel_cambiarCategoria.add(txt_cambiarCategoria_nombre);
		
		JButton btnCambiar_categoria = new JButton("Aplicar");
		btnCambiar_categoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (verifica(txt_cambiarCategoria_id.getText()) && verifica(txt_cambiarCategoria_nombre.getText())) {
					String data = PostgreSQLJDBC.consulta(2, new String[] {txt_cambiarCategoria_id.getText(), txt_cambiarCategoria_nombre.getText()});
					JOptionPane.showMessageDialog(null, data);
				} else {
					JOptionPane.showMessageDialog(null, "Complete todos los campos.");
				}
			}
		});
		btnCambiar_categoria.setBounds(206, 204, 88, 25);
		panel_cambiarCategoria.add(btnCambiar_categoria);
		
		JButton btnLimpiar_cambiarCategoria = new JButton("Limpiar");
		btnLimpiar_cambiarCategoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_cambiarCategoria_id.setText("");
				txt_cambiarCategoria_nombre.setText("");
			}
		});
		btnLimpiar_cambiarCategoria.setBounds(304, 204, 69, 25);
		panel_cambiarCategoria.add(btnLimpiar_cambiarCategoria);
		
		JPanel panel_busquedaActores = new JPanel();
		panel_busquedaActores.setLayout(null);
		panel_busquedaActores.setBackground(Color.WHITE);
		panel.add(panel_busquedaActores, "panel_busqueda_Actores");
		
		JLabel lblBusquedaDeActores = new JLabel("Busqueda de actores");
		lblBusquedaDeActores.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblBusquedaDeActores.setBounds(15, 8, 177, 20);
		panel_busquedaActores.add(lblBusquedaDeActores);
		
		JSeparator separator_6 = new JSeparator();
		separator_6.setBounds(10, 35, 372, 2);
		panel_busquedaActores.add(separator_6);
		
		JLabel lblApellido_1 = new JLabel("Apellido:");
		lblApellido_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblApellido_1.setBounds(10, 50, 59, 14);
		panel_busquedaActores.add(lblApellido_1);
		
		txt_busquedaApellido = new JTextField();
		txt_busquedaApellido.setColumns(10);
		txt_busquedaApellido.setBounds(79, 45, 215, 25);
		panel_busquedaActores.add(txt_busquedaApellido);
		
		JButton btnBuscarActor = new JButton("Buscar");
		btnBuscarActor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Object modelo[][] = PostgreSQLJDBC.obtenerDatos(1, "%"+txt_busquedaApellido.getText()+"%");
					if (modelo != null) {
						table_actores.setModel(new DefaultTableModel(modelo, new String[] {"ID", "Nombre", "Apellido"}));
					} else {
						JOptionPane.showMessageDialog(null, "No se encontraron coincidencias.");
					}
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "No se ha podido realizar la tarea.");
				}
			}
		});
		btnBuscarActor.setBounds(304, 45, 69, 25);
		panel_busquedaActores.add(btnBuscarActor);
		
		JScrollPane scrollPane_actor = new JScrollPane();
		scrollPane_actor.setBounds(20, 80, 353, 149);
		panel_busquedaActores.add(scrollPane_actor);
		
		table_actores = new JTable();
		table_actores.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"ID", "Nombre", "Apellido"
			}
		));
		scrollPane_actor.setViewportView(table_actores);
		
		JPanel panel_busquedaExistencia = new JPanel();
		panel_busquedaExistencia.setLayout(null);
		panel_busquedaExistencia.setBackground(Color.WHITE);
		panel.add(panel_busquedaExistencia, "panel_busqueda_Existencia");
		
		JLabel lblBuscarExistencias = new JLabel("Buscar existencias");
		lblBuscarExistencias.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblBuscarExistencias.setBounds(15, 8, 177, 20);
		panel_busquedaExistencia.add(lblBuscarExistencias);
		
		JSeparator separator_7 = new JSeparator();
		separator_7.setBounds(10, 35, 372, 2);
		panel_busquedaExistencia.add(separator_7);
		
		JLabel lblTitulo = new JLabel("Titulo:");
		lblTitulo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTitulo.setBounds(10, 50, 59, 14);
		panel_busquedaExistencia.add(lblTitulo);
		
		txt_busquedaTitulo = new JTextField();
		txt_busquedaTitulo.setColumns(10);
		txt_busquedaTitulo.setBounds(79, 45, 215, 25);
		panel_busquedaExistencia.add(txt_busquedaTitulo);
		
		JButton btnBuscarExistencia = new JButton("Buscar");
		btnBuscarExistencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Object modelo[][] = PostgreSQLJDBC.obtenerDatos(2, "%"+txt_busquedaTitulo.getText()+"%");
					if (modelo != null) {
						table_existencia.setModel(new DefaultTableModel(modelo, new String[] {"Tienda", "Nombre Pelicula", "Existencia"}));
					} else {
						JOptionPane.showMessageDialog(null, "No se encontraron coincidencias.");
					}
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(null, "No se ha podido realizar la tarea.");
				}
			}
		});
		btnBuscarExistencia.setBounds(304, 45, 69, 25);
		panel_busquedaExistencia.add(btnBuscarExistencia);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(20, 80, 353, 149);
		panel_busquedaExistencia.add(scrollPane_1);
		
		table_existencia = new JTable();
		table_existencia.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
			},
			new String[] {
				"Tienda", "Nombre Pelicula", "Existencia"
			}
		));
		scrollPane_1.setViewportView(table_existencia);
		
		JPanel panel_rentar = new JPanel();
		panel_rentar.setLayout(null);
		panel_rentar.setBackground(Color.WHITE);
		panel.add(panel_rentar, "panel_rentar");
		
		JLabel lblRentar = new JLabel("Rentar");
		lblRentar.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblRentar.setBounds(15, 8, 177, 20);
		panel_rentar.add(lblRentar);
		
		JSeparator separator_8 = new JSeparator();
		separator_8.setBounds(10, 35, 372, 2);
		panel_rentar.add(separator_8);
		
		JLabel lblIdInventario = new JLabel("ID inventario:");
		lblIdInventario.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdInventario.setBounds(10, 50, 79, 14);
		panel_rentar.add(lblIdInventario);
		
		txt_renta_idInventario = new JTextField();
		txt_renta_idInventario.setColumns(10);
		txt_renta_idInventario.setBounds(99, 45, 273, 25);
		panel_rentar.add(txt_renta_idInventario);
		
		JLabel lblIdCliente = new JLabel("ID cliente:");
		lblIdCliente.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdCliente.setBounds(10, 86, 79, 14);
		panel_rentar.add(lblIdCliente);
		
		txt_renta_idCliente = new JTextField();
		txt_renta_idCliente.setColumns(10);
		txt_renta_idCliente.setBounds(99, 81, 273, 25);
		panel_rentar.add(txt_renta_idCliente);
		
		JLabel lblIdTrabajador = new JLabel("ID Trabajador:");
		lblIdTrabajador.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdTrabajador.setBounds(10, 122, 81, 14);
		panel_rentar.add(lblIdTrabajador);
		
		txt_renta_idTrabajador = new JTextField();
		txt_renta_idTrabajador.setColumns(10);
		txt_renta_idTrabajador.setBounds(99, 117, 273, 25);
		panel_rentar.add(txt_renta_idTrabajador);
		
		JButton btnRegistrarRenta = new JButton("Registrar");
		btnRegistrarRenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (verifica(txt_renta_idInventario.getText()) && verifica(txt_renta_idCliente.getText()) && verifica(txt_renta_idTrabajador.getText())) {
					String data = PostgreSQLJDBC.consulta(7, new String[] {txt_renta_idInventario.getText(), txt_renta_idCliente.getText(), txt_renta_idTrabajador.getText()});
					JOptionPane.showMessageDialog(null, data);
				} else {
					JOptionPane.showMessageDialog(null, "Complete todos los campos.");
				}
			}
		});
		btnRegistrarRenta.setBounds(205, 204, 88, 25);
		panel_rentar.add(btnRegistrarRenta);
		
		JButton btnLimpiarRenta = new JButton("Limpiar");
		btnLimpiarRenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txt_renta_idInventario.setText("");
				txt_renta_idCliente.setText("");
				txt_renta_idTrabajador.setText("");
			}
		});
		btnLimpiarRenta.setBounds(303, 204, 69, 25);
		panel_rentar.add(btnLimpiarRenta);
		
		JPanel panel_devolver = new JPanel();
		panel_devolver.setLayout(null);
		panel_devolver.setBackground(Color.WHITE);
		panel.add(panel_devolver, "panel_devolver");
		
		JLabel lblDevolverRenta = new JLabel("Devolver Renta");
		lblDevolverRenta.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblDevolverRenta.setBounds(15, 8, 177, 20);
		panel_devolver.add(lblDevolverRenta);
		
		JSeparator separator_9 = new JSeparator();
		separator_9.setBounds(10, 35, 372, 2);
		panel_devolver.add(separator_9);
		
		JLabel label = new JLabel("ID inventario:");
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(15, 50, 79, 14);
		panel_devolver.add(label);
		
		txt_devolucion_idInventario = new JTextField();
		txt_devolucion_idInventario.setColumns(10);
		txt_devolucion_idInventario.setBounds(104, 45, 273, 25);
		panel_devolver.add(txt_devolucion_idInventario);
		
		JLabel label_4 = new JLabel("ID cliente:");
		label_4.setHorizontalAlignment(SwingConstants.RIGHT);
		label_4.setBounds(15, 86, 79, 14);
		panel_devolver.add(label_4);
		
		txt_devolucion_idCliente = new JTextField();
		txt_devolucion_idCliente.setColumns(10);
		txt_devolucion_idCliente.setBounds(104, 81, 273, 25);
		panel_devolver.add(txt_devolucion_idCliente);
		
		JLabel label_6 = new JLabel("ID Trabajador:");
		label_6.setHorizontalAlignment(SwingConstants.RIGHT);
		label_6.setBounds(15, 122, 81, 14);
		panel_devolver.add(label_6);
		
		txt_devolucion_idTrabajador = new JTextField();
		txt_devolucion_idTrabajador.setColumns(10);
		txt_devolucion_idTrabajador.setBounds(104, 117, 273, 25);
		panel_devolver.add(txt_devolucion_idTrabajador);
		
		JButton btnRegistrarBaja = new JButton("Registrar Devolucion");
		btnRegistrarBaja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (verifica(txt_devolucion_idInventario.getText()) && verifica(txt_devolucion_idCliente.getText()) && verifica(txt_devolucion_idTrabajador.getText())) {
					String data = PostgreSQLJDBC.consulta(8, new String[] {txt_devolucion_idInventario.getText(), txt_devolucion_idCliente.getText(), txt_devolucion_idTrabajador.getText()});
					JOptionPane.showMessageDialog(null, data);
				} else {
					JOptionPane.showMessageDialog(null, "Complete todos los campos.");
				}
			}
		});
		btnRegistrarBaja.setBounds(172, 204, 123, 25);
		panel_devolver.add(btnRegistrarBaja);
		
		JButton btn_limpiarDevolver = new JButton("Limpiar");
		btn_limpiarDevolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txt_devolucion_idInventario.setText("");
				txt_devolucion_idCliente.setText("");
				txt_devolucion_idTrabajador.setText("");
			}
		});
		btn_limpiarDevolver.setBounds(305, 204, 69, 25);
		panel_devolver.add(btn_limpiarDevolver);
		frame.setBounds(100, 100, 406, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnRentas = new JMenu("  Rentas  ");
		menuBar.add(mnRentas);
		
		menuRentarRentas = new JRadioButtonMenuItem("Rentar");
		menuRentarRentas.addActionListener(opcion);
		menuPrincipal.add(menuRentarRentas);
		mnRentas.add(menuRentarRentas);
		
		menuDevolverRentas = new JRadioButtonMenuItem("Devolver");
		menuDevolverRentas.addActionListener(opcion);
		menuPrincipal.add(menuDevolverRentas);
		mnRentas.add(menuDevolverRentas);
		
		JMenu mnBusqueda = new JMenu("  Busqueda  ");
		menuBar.add(mnBusqueda);
		
		menuActoresBusqueda = new JRadioButtonMenuItem("Actores");
		menuActoresBusqueda.addActionListener(opcion);
		menuPrincipal.add(menuActoresBusqueda);
		mnBusqueda.add(menuActoresBusqueda);
		
		menuExistenciaBusqueda = new JRadioButtonMenuItem("Exitencia");
		menuExistenciaBusqueda.addActionListener(opcion);
		menuPrincipal.add(menuExistenciaBusqueda);
		mnBusqueda.add(menuExistenciaBusqueda);
		
		JMenu mnActores = new JMenu("  Actores  ");
		menuBar.add(mnActores);
		
		menuAltaActores = new JRadioButtonMenuItem("Alta");
		menuAltaActores.addActionListener(opcion);
		menuPrincipal.add(menuAltaActores);
		mnActores.add(menuAltaActores);
		
		menuBajaActores = new JRadioButtonMenuItem("Baja");
		menuBajaActores.addActionListener(opcion);
		menuPrincipal.add(menuBajaActores);
		mnActores.add(menuBajaActores);
		
		menuCambiarActores = new JRadioButtonMenuItem("Cambiar");
		menuCambiarActores.addActionListener(opcion);
		menuPrincipal.add(menuCambiarActores);
		mnActores.add(menuCambiarActores);
		
		JMenu mnCategorias = new JMenu("  Categorias  ");
		menuBar.add(mnCategorias);
		
		menuAltaCategoria = new JRadioButtonMenuItem("Alta");
		menuAltaCategoria.addActionListener(opcion);
		menuPrincipal.add(menuAltaCategoria);
		mnCategorias.add(menuAltaCategoria);
		
		menuBajaCategoria = new JRadioButtonMenuItem("Baja");
		menuBajaCategoria.addActionListener(opcion);
		menuPrincipal.add(menuBajaCategoria);
		mnCategorias.add(menuBajaCategoria);
		
		menuCambiarCategoria = new JRadioButtonMenuItem("Cambiar");
		menuCambiarCategoria.addActionListener(opcion);
		menuPrincipal.add(menuCambiarCategoria);
		mnCategorias.add(menuCambiarCategoria);
	}
	
	// Cambiar CARDLAYOUT
	public static void cambiarcapa(Container capa, String nombrecontenedor) {
		CardLayout cl = (CardLayout) capa.getLayout();
		cl.show(capa, nombrecontenedor);
	}
	
	public static Boolean verifica(String s) {
		if (s.replaceAll(" ", "").equals(""))
			return false;
		return true;
	}

	class selMenu implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JRadioButtonMenuItem cual = (JRadioButtonMenuItem) e.getSource();
			if (cual == menuRentarRentas)
				cambiarcapa(panel, "panel_rentar");
			else if (cual == menuDevolverRentas)
				cambiarcapa(panel, "panel_devolver");
			else if (cual == menuActoresBusqueda)
				cambiarcapa(panel, "panel_busqueda_Actores");
			else if (cual == menuExistenciaBusqueda)
				cambiarcapa(panel, "panel_busqueda_Existencia");
			else if (cual == menuAltaActores)
				cambiarcapa(panel, "panel_alta_Actor");
			else if (cual == menuBajaActores)
				cambiarcapa(panel, "panel_baja_Actor");
			else if (cual == menuCambiarActores)
				cambiarcapa(panel, "panel_cambiar_Actor");
			else if (cual == menuAltaCategoria)
				cambiarcapa(panel, "panel_alta_Categoria");
			else if (cual == menuBajaCategoria)
				cambiarcapa(panel, "panel_baja_Categoria");
			else if (cual == menuCambiarCategoria)
				cambiarcapa(panel, "panel_cambiar_Categoria");
		}
	}
}
