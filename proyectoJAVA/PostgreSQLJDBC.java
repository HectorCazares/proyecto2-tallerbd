package proyectoJAVA;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PostgreSQLJDBC {
	
	static Connection con = null;

	public static String consulta(int opcion, String[] objects) {
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DVDrental", "postgres", "123456789");
			CallableStatement cstmt = null;
			switch (opcion) {
			case 1:
				cstmt = con.prepareCall("{call agrega_categoria(?)}");
				cstmt.setString(1, objects[0]);
				break;
			case 2:
				cstmt = con.prepareCall("{call update_categoria(?,?)}");
				cstmt.setInt(1, Integer.parseInt(objects[0]));
				cstmt.setString(2, objects[1]);
				break;
			case 3:
				cstmt = con.prepareCall("{call delete_categoria(?)}");
				cstmt.setInt(1, Integer.parseInt(objects[0]));
				break;
			case 4:
				cstmt = con.prepareCall("{call agrega_actor(?,?)}");
				cstmt.setString(1, objects[0]);
				cstmt.setString(2, objects[1]);
				break;
			case 5:
				cstmt = con.prepareCall("{call update_actor(?,?,?)}");
				cstmt.setInt(1, Integer.parseInt(objects[0]));
				cstmt.setString(2, objects[1]);
				cstmt.setString(3, objects[2]);
				break;
			case 6:
				cstmt = con.prepareCall("{call delete_actor(?)}");
				cstmt.setInt(1, Integer.parseInt(objects[0]));
				break;
			case 7:
				cstmt = con.prepareCall("{call alta_renta(?,?,?)}");
				cstmt.setInt(1, Integer.parseInt(objects[0]));
				cstmt.setInt(2, Integer.parseInt(objects[1]));
				cstmt.setInt(3, Integer.parseInt(objects[2]));
				break;
			case 8:
				cstmt = con.prepareCall("{call baja_renta(?,?,?)}");
				cstmt.setInt(1, Integer.parseInt(objects[0]));
				cstmt.setInt(2, Integer.parseInt(objects[1]));
				cstmt.setInt(3, Integer.parseInt(objects[2]));
				break;
			default:
				return "El procedimiento a llamar no existe.";
			}
			
			cstmt.execute();
			if (cstmt.getWarnings() != null) {
				cstmt.close();
				return cstmt.getWarnings().getMessage();
			} else {
				cstmt.close();
				return "operacion exitosa.";
			}
		} catch (SQLException es) {
			return es.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (con != null)try {con.close();} catch (Exception e) {}
		}
		return null;
	}
	
	public static Object[][] obtenerDatos(int opcion, String str) {
		try {
			Class.forName("org.postgresql.Driver");
			Object modelo[][] = null;
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DVDrental", "postgres", "123456789");
			PreparedStatement pstmt = null;
			switch (opcion) {
			case 1:
				pstmt = con.prepareStatement("SELECT * FROM get_actor (?)");
				pstmt.setString(1, str);
				ResultSet rs = pstmt.executeQuery();
				modelo = new Object[50][3];
				int i = 0, j = 0;
		        while (rs.next() && i < 50) {
		        	modelo[i][j++] = rs.getInt("id");
		        	modelo[i][j++] = rs.getString("nombre");
		        	modelo[i][j++] = rs.getString("apellido");
		        	j = 0;
		        	i++;
		         }
		         rs.close();
				break;
			case 2:
				pstmt = con.prepareStatement("SELECT * FROM get_existencia (?)");
				pstmt.setString(1, str);
				ResultSet rs1 = pstmt.executeQuery();
				modelo = new Object[50][3];
				int i1 = 0, j1 = 0;
		        while (rs1.next() && i1 < 50) {
		        	modelo[i1][j1++] = rs1.getInt("id");
		        	modelo[i1][j1++] = rs1.getString("titulo");
		        	modelo[i1][j1++] = rs1.getInt("total");
		        	j1 = 0;
		        	i1++;
		         }
		         rs1.close();
				break;
			default:
				return null;
			}
			
			if (pstmt.getWarnings() != null) {
				pstmt.close();
				return null;
			} else {
				pstmt.close();
				return modelo;
			}
		} catch (SQLException es) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (con != null)try {con.close();} catch (Exception e) {}
		}
		return null;
	}
}